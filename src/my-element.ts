import { html, css, LitElement } from "lit";
import { customElement, property } from "lit/decorators.js";

/**
 * An example element.
 *
 * @slot - This element has a slot
 * @csspart button - The button
 */
@customElement("my-element")
export class MyElement extends LitElement {
  static styles = css`
    :host {
      --application-left: 20px;
      --application-right: 20px;
    }

    .appHeader {
      position: absolute;
      left: var(--application-left, 20px);
      top: 20px;
      right: var(--application-right, 20px);
      z-index: 10;
      height: 62px;
      line-height: 62px;
      font-size: 30px;
      background: #aaffaa;
      display: flex;
    }

    .appHeader .label {
      flex: 1;
    }

    .appHeader .explore {
    }

    .breadcrumb {
      position: absolute;
      top: 86px; /* 4px below header bottom */
      left: var(--application-left, 20px);
      max-width: 440px;
      min-height: 52px;
      z-index: 8;
      background: #ffffaa;
      line-height: 24px;
      display: flex;
    }

    .searchField {
      position: absolute;
      top: var(--breadcrumb-bottom, 146px);
      left: var(--application-left, 20px);
      width: 440px;
      background: #ffffaa;
      z-index: 8;
      height: 50px;
    }

    .mainContent {
      position: absolute;
      font-size: 40px;
      background: #aaaaff;
      padding-left: 500px;
      padding-top: 45vh;
      bottom: 0px;
      left: 0px;
      top: 0px;
      right: 0px;
    }

    .map {
    }

    .table,
    .graph {
      left: var(--content-left, 0px);
    }

    button {
      position: absolute;
      width: 50px;
      height: 50px;
      z-index: 10;
    }

    .mtSwitch {
      top: 86px;
      right: 130px;
    }

    .expand {
      top: 86px;
      right: 20px;
    }

    .near {
      top: 307px;
      right: 20px;
    }

    .info {
      top: 417px;
      right: 20px;
    }

    .graph .expand {
      top: 86px;
      right: 75px;
    }

    .graph .graphClose {
      top: 86px;
      right: 20px;
    }

    .sidebar {
      position: absolute;
      width: 440px;
      bottom: 0px;
      left: 0px;
      top: 0px;
      background: #ffaaaa;
      z-index: 5;
      padding-top: var(--search-bottom, 146px);
      display: flex;
      flex-direction: column;
      padding-left: var(--application-left, 20px);
      padding-right: var(
        --application-left,
        20px
      ); /* exqual padding on both sides */
    }

    .sidebar.overlay {
      padding-top: 86px;
      z-index: 9;
    }

    .sidebar .header {
      display: flex;
    }

    .sidebar .title {
      flex: 1;
      line-height: 50px;
    }

    .sidebar .sidebarClose {
      position: relative;
    }

    .sidebar .topics,
    .sidebar .catchments,
    .sidebar .stations,
    .sidebar .graphs {
      padding-top: 10px;
      padding-bottom: 10px;
    }

    .sidebar .topic,
    .sidebar .catchment,
    .sidebar .station,
    .sidebar .graph {
      height: 50px;
      width: 100%;
      border: 1px solid;
      text-align: center;
      line-height: 50px;
    }
  `;

  @property({ type: Boolean })
  sidebarActive = true;

  // Some sidebars will hide the search field
  @property({ type: Boolean })
  searchActive = true;

  @property({ type: String })
  content = "map";

  // Used for restoring the content value
  @property({ type: String })
  mainContent = "map";
  /**
   * The screen position of the bottom of the breadcrumb + a bit of padding in pixels
   */
  @property({ type: Number })
  breadcrumbBottom = 146;

  @property({ type: Array })
  activeSidebar = [];

  @property({ type: Object })
  sidebarOverlay = null;

  searchBottom() {
    if (this.searchActive) {
      return this.breadcrumbBottom + 50 + 8; // 50 for search height and 8 for padding
    }
    return this.breadcrumbBottom;
  }

  render() {
    return html`
      <style>
        :host {
          --breadcrumb-bottom: ${this.breadcrumbBottom}px;
          --search-bottom: ${this.searchBottom()}px;
          --content-left: ${this.sidebarActive ? "480px" : "0px"};
        }
      </style>
      ${this.renderSidebar()}
      <div class="appHeader">
        <div class="label">Header</div>
        <div class="explore" @click=${this.resetApplication}>Explore Data</div>
      </div>
      <div class="breadcrumb">${this.breadcrumbText()}</div>
      ${this.renderSearch()} ${this.renderMap()} ${this.renderTable()}
      ${this.renderGraph()}
    `;
  }

  resetApplication() {
    this.activeSidebar = [];
    this.content = "map";
    this.sidebarOverlay = null;
    this.requestUpdate();
  }

  breadcrumbText() {
    const stackSize = this.activeSidebar.length - 1;
    return html`Home >&nbsp
      <div @click=${this.resetApplication}>Explore</div>
      ${this.activeSidebar.map(
        (item, index) =>
          html`&nbsp>&nbsp
            <div
              @click=${() => {
                for (let i = index; i < stackSize; i++) {
                  this.activeSidebar.pop();
                }
                this.requestUpdate();
              }}
            >
              ${item.name}
            </div>`
      )}`;
  }

  renderMap() {
    if (this.content !== "map") {
      return "";
    }
    return html` <div class="map mainContent">
      Map
      <button class="mtSwitch" @click=${this.openTable}>T</button>
      <button class="near" @click=${this.toggleNearSidebar}>N</button>
      <button class="info" @click=${this.toggleInfoSidebar}>I</button>
      <button class="expand" @click=${this.toggleSidebar}>X</button>
    </div>`;
  }

  renderTable() {
    if (this.content !== "table") {
      return "";
    }
    return html` <div class="table mainContent">
      Table
      <button class="mtSwitch" @click=${this.openMap}>M</button>
      <button class="expand" @click=${this.toggleSidebar}>X</button>
    </div>`;
  }

  renderGraph() {
    if (this.content !== "graph") {
      return "";
    }
    return html` <div class="graph mainContent">
      Graph
      <button class="expand" @click=${this.toggleSidebar}>X</button>
      <button class="graphClose" @click=${this.closeGraph}>C</button>
    </div>`;
  }

  closeSidebar() {
    this.activeSidebar.pop();
    this.requestUpdate();
  }

  renderSidebar() {
    if (!this.sidebarActive) {
      return "";
    }
    if (this.sidebarOverlay) {
      return this.sidebarOverlay.element;
    }
    if (this.activeSidebar.length <= 0) {
      return html`
        <div class="sidebar">
          <div class="header">
            <div class="title">Sidebar</div>
          </div>
          <div class="topics">
            Topics
            <div
              class="topic"
              @click=${() => this.openTopicSidebar("Surface Water")}
            >
              Surface Water
            </div>
            <div
              class="topic"
              @click=${() => this.openTopicSidebar("Groundwater")}
            >
              Groundwater
            </div>
          </div>
          <div class="catchments">
            Catchments/RWC/River Basins
            <div
              class="catchment"
              @click=${() => this.openCatchmentSidebar("Carangamite")}
            >
              Carangamite
            </div>
            <div
              class="catchment"
              @click=${() => this.openCatchmentSidebar("Gippsland")}
            >
              Gippsland
            </div>
            <div
              class="catchment"
              @click=${() => this.openCatchmentSidebar("Glenelg")}
            >
              Glenelg
            </div>
          </div>
        </div>
      `;
    }
    return this.activeSidebar[this.activeSidebar.length - 1].element;
  }

  openTopicSidebar(topic) {
    this.activeSidebar.push({
      name: topic,
      element: html`
        <div class="sidebar">
          <div class="header">
            <button class="sidebarClose" @click=${this.closeSidebar}>B</button>
            <div class="title">${topic}</div>
          </div>
          <div class="catchments">
            Catchments/RWC/River Basins
            <div
              class="catchment"
              @click=${() => this.openCatchmentSidebar("Carangamite")}
            >
              Carangamite
            </div>
            <div
              class="catchment"
              @click=${() => this.openCatchmentSidebar("Gippsland")}
            >
              Gippsland
            </div>
            <div
              class="catchment"
              @click=${() => this.openCatchmentSidebar("Glenelg")}
            >
              Glenelg
            </div>
          </div>
        </div>
      `,
    });
    this.requestUpdate();
  }

  openCatchmentSidebar(catchment) {
    this.activeSidebar.push({
      name: catchment,
      element: html`
        <div class="sidebar">
          <div class="header">
            <button class="sidebarClose" @click=${this.closeSidebar}>B</button>
            <div class="title">${catchment}</div>
          </div>
          <div class="stations">
            Stations
            <div
              class="station"
              @click=${() => this.openStationSidebar("223202")}
            >
              223202
            </div>
            <div
              class="station"
              @click=${() => this.openStationSidebar("222209")}
            >
              222209
            </div>
            <div
              class="station"
              @click=${() => this.openStationSidebar("221208")}
            >
              221208
            </div>
          </div>
        </div>
      `,
    });
    this.requestUpdate();
  }

  openStationSidebar(station) {
    this.sidebarActive = true;
    this.sidebarOverlay = {
      name: station,
      element: html`
        <div class="sidebar overlay">
          <div class="header">
            <div class="title">${station}</div>
            <button
              class="sidebarClose"
              @click=${() => {
                this.closeGraph();
                this.closeSidebarOverlay();
              }}
            >
              C
            </button>
          </div>
          <div class="graphs">
            Graphs
            <div class="graph" @click=${() => this.openGraph("Graph 1")}>
              Graph 1
            </div>
            <div class="graph" @click=${() => this.openGraph("Graph 2")}>
              Graph 2
            </div>
            <div class="graph" @click=${() => this.openGraph("Graph 3")}>
              Graph 3
            </div>
          </div>
        </div>
      `,
    };
  }

  openGraph(graphID) {
    this.content = "graph";
    // this.some other property = graphID and other details
  }

  closeGraph() {
    this.content = this.mainContent;
  }

  renderSearch() {
    if (!this.searchActive) {
      return "";
    }
    return html` <div class="searchField">Search</div>`;
  }

  openMap() {
    this.content = "map";
    this.mainContent = "map";
  }

  openTable() {
    this.content = "table";
    this.mainContent = "table";
  }

  toggleSidebar() {
    this.sidebarActive = !this.sidebarActive;
  }

  toggleNearSidebar() {
    this.sidebarActive = true;
    this.sidebarOverlay = {
      name: "Near Me",
      element: html`
        <div class="sidebar overlay">
          <div class="header">
            <div class="title">Near Me</div>
            <button class="sidebarClose" @click=${this.closeSidebarOverlay}>
              C
            </button>
          </div>
        </div>
      `,
    };
  }

  toggleInfoSidebar() {
    this.sidebarActive = true;
    this.sidebarOverlay = {
      name: "Info Pin",
      element: html`
        <div class="sidebar overlay">
          <div class="header">
            <div class="title">Info Pin</div>
            <button class="sidebarClose" @click=${this.closeSidebarOverlay}>
              C
            </button>
          </div>
        </div>
      `,
    };
  }

  closeSidebarOverlay() {
    this.sidebarOverlay = null;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    "my-element": MyElement;
  }
}
